from discord.ext import commands
import discord
import os
from datetime import datetime
import asyncio
import utils

class createPersonalChannel(commands.Cog):
	def __init__(self, bot):
		self.bot = bot

	@commands.command(name="create")
	async def createPersonalChannel_command(self, ctx,roleid,categoryid):
		if ctx.message.guild:
			
			config = utils.read_json("configs/config.json")
			
			if str(ctx.message.author.id) in config["QUESTIONNAIRE"]["CLIENT_ID"]:
				
				#categoryChannelObject = await self.bot.fetch_channel(int(categoryid))
				#guild = categoryChannelObject.guild
				categoryChannelObject = discord.utils.get(ctx.message.guild.categories, id=int(categoryid))
				guild = ctx.message.guild
				if categoryChannelObject is None:
					await ctx.send("Error channel category with id "+str(categoryid)+" needs exist on the server")
					return False
				
				#everyoneRole = discord.utils.get(guild.roles, name="@everyone")
				
				for guildobj in self.bot.guilds:
					role = guildobj.get_role(int(roleid))
					if not role is None:
						break
					
				if role is None:
					await ctx.send("Invalid role id")
					return False
				
				'''
				overwrites_questionnaire = {
					everyoneRole:discord.PermissionOverwrite(create_instant_invite=False,kick_members=False,ban_members=False,administrator=False,manage_channels=False,manage_guild=False,add_reactions=False,view_audit_log=False,priority_speaker=False,read_messages=False,send_messages=False,send_tts_messages=False,manage_messages=False,embed_links=False,attach_files=False,read_message_history=False,mention_everyone=False,external_emojis=False,connect=False,speak=False,mute_members=False,deafen_members=False,move_members=False,use_voice_activation=False,change_nickname=False,manage_nicknames=False,manage_roles=False,manage_webhooks=False,manage_emojis=False),
					role:discord.PermissionOverwrite(create_instant_invite=False,kick_members=False,ban_members=False,administrator=False,manage_channels=False,manage_guild=False,add_reactions=False,view_audit_log=False,priority_speaker=False,read_messages=True,send_messages=True,send_tts_messages=False,manage_messages=False,embed_links=True,attach_files=True,read_message_history=True,mention_everyone=False,external_emojis=False,connect=False,speak=False,mute_members=False,deafen_members=False,move_members=False,use_voice_activation=False,change_nickname=False,manage_nicknames=False,manage_roles=False,manage_webhooks=False,manage_emojis=False)
				}
				'''
				
				'''
				overwrites_questionnaire = {
					everyoneRole:discord.PermissionOverwrite(create_instant_invite=False,kick_members=False,ban_members=False,administrator=False,manage_channels=False,manage_guild=False,add_reactions=False,view_audit_log=False,priority_speaker=False,read_messages=False,send_messages=False,send_tts_messages=False,manage_messages=False,embed_links=False,attach_files=False,read_message_history=False,mention_everyone=False,external_emojis=False,connect=False,speak=False,mute_members=False,deafen_members=False,move_members=False,use_voice_activation=False,change_nickname=False,manage_nicknames=False,manage_roles=False,manage_webhooks=False,manage_emojis=False),
					everyoneRole:discord.PermissionOverwrite(create_instant_invite=False,kick_members=False,ban_members=False,administrator=False,manage_channels=False,manage_guild=False,add_reactions=False,view_audit_log=False,priority_speaker=False,read_messages=True,send_messages=True,send_tts_messages=False,manage_messages=False,embed_links=True,attach_files=True,read_message_history=True,mention_everyone=False,external_emojis=False,connect=False,speak=False,mute_members=False,deafen_members=False,move_members=False,use_voice_activation=False,change_nickname=False,manage_nicknames=False,manage_roles=False,manage_webhooks=False,manage_emojis=False)
				}
				
				ctx.message.author:discord.PermissionOverwrite(create_instant_invite=False,kick_members=False,ban_members=False,administrator=False,manage_channels=False,manage_guild=False,add_reactions=False,view_audit_log=False,priority_speaker=False,read_messages=True,send_messages=True,send_tts_messages=False,manage_messages=False,embed_links=True,attach_files=True,read_message_history=True,mention_everyone=False,external_emojis=False,connect=False,speak=False,mute_members=False,deafen_members=False,move_members=False,use_voice_activation=False,change_nickname=False,manage_nicknames=False,manage_roles=False,manage_webhooks=False,manage_emojis=False)
				'''
				
				for member in role.members:
					if not str(member.id) in utils.memberChannels:
						channel_created = await guild.create_text_channel(str(member),category=categoryChannelObject)
					
						await asyncio.sleep(3) #Creating channels are not instant
					
						utils.memberChannels[str(member.id)] = str(channel_created.id)
						utils.save_json("configs/memberChannels.json",utils.memberChannels)
				
				try:
					await ctx.send("Personal channels created")
				except:
					pass
			
def setup(bot):
	bot.add_cog(createPersonalChannel(bot))